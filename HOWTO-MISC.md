Miscellaneous notes:

* Generating POT Files: Use [WP CLI](https://make.wordpress.org/cli/handbook/). `wp i18n make-pot . wp_nobot_question.pot`
  http://www.compdigitec.com/labs/2020/07/06/generating-pot-files-for-wordpress-plugins/
* Updating the change log: `git log --oneline --decorate` (plus manual editing)

